module.exports = {
  dev: {
    bsFiles: {
        src : [
        	'<%= globalConfig.dest %>/**/*',
          '!<%= globalConfig.dest %>/**/*.min.js',
          '!<%= globalConfig.dest %>/**/*.map',
          '_templates/**/*.html',
        	'_templates/**/*.twig'
        ]
    },
    options: {
        xip: "<%= localConfig.xip %>",
      	watchTask: true,
        proxy: "<%= localConfig.domain %>"
    }
  }
}
