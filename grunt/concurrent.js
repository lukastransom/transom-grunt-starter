module.exports = {

  // Task options
  options: {
    limit: 5
  },

  // Prep the build stage
  buildFirst: [
    'clean',
  ],
  // Build & compile assets
  buildSecond: [
    'copy',
    'sass',
    'browserify',
    'svgstore',
    'imagemin'
  ],
  // Run cleanup tasks on compiled assets
  buildThird: [
    'autoprefixer',
    'uglify'
  ]
};