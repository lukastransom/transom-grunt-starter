/**
 * Customize these to your hearts content
 */
module.exports = {
  // https://github.com/jedrichards/grunt-rsync
  options: {
      args: ["--verbose"],
      exclude: ["storage/runtime", ".git*","*.scss","node_modules",".DS_Store",".DS_Store?","*.sample","ignore-me","._*",".Spotlight-V100",".Trashes","ehthumbs.db","Thumbs.db",".gitignore"],
      recursive: true
  },
  // ===============
  //   Development
  // ===============
  push_dev_craft: {
    options: {
      delete: true,
      src: "<%= rsync.local.craft %>/",
      dest: "<%= rsync.dev.craft %>/",
    }
  },
  push_dev_config: {
    options: {
      delete: true,
      exclude: ["*local"],
      src: "<%= rsync.local.config %>/",
      dest: "<%= rsync.dev.config %>/",
    }
  },
  push_dev_plugins: {
    options: {
      delete: true,
      src: "<%= rsync.local.plugins %>/",
      dest: "<%= rsync.dev.plugins %>/",
    }
  },
  push_dev_templates: {
    options: {
      delete: true,
      src: "<%= rsync.local.templates %>/",
      dest: "<%= rsync.dev.templates %>/",
    }
  },
  push_dev_public: {
    options: {
      src: "<%= rsync.local.public %>/",
      dest: "<%= rsync.dev.public %>/",
    }
  },
  push_dev_public_delete: {
    options: {
      delete: true,
      src: "<%= rsync.local.public %>/",
      dest: "<%= rsync.dev.public %>/",
    }
  },
  push_dev_assets: {
    options: {
      delete: true,
      src: "<%= rsync.local.assets %>/",
      dest: "<%= rsync.dev.assets %>/",
    }
  },
  push_dev_uploads: {
    options: {
      src: "<%= rsync.local.uploads %>/",
      dest: "<%= rsync.dev.uploads %>/"
    }
  },
  pull_dev_uploads: {
    options: {
      src: "<%= rsync.dev.uploads %>/",
      dest: "<%= rsync.local.uploads %>/",
    }
  },
  // ==============
  //   Production
  // ==============
  push_prod_craft: {
    options: {
      delete: true,
      src: "<%= rsync.local.craft %>/",
      dest: "<%= rsync.prod.craft %>/",
    }
  },
  push_prod_config: {
    options: {
      delete: true,
      exclude: ["*local"],
      src: "<%= rsync.local.config %>/",
      dest: "<%= rsync.prod.config %>/",
    }
  },
  push_prod_plugins: {
    options: {
      delete: true,
      src: "<%= rsync.local.plugins %>/",
      dest: "<%= rsync.prod.plugins %>/",
    }
  },
  push_prod_templates: {
    options: {
      delete: true,
      src: "<%= rsync.local.templates %>/",
      dest: "<%= rsync.prod.templates %>/",
    }
  },
  push_prod_public: {
    options: {
      src: "<%= rsync.local.public %>/",
      dest: "<%= rsync.prod.public %>/",
    }
  },
  push_prod_public_delete: {
    options: {
      delete: true,
      src: "<%= rsync.local.public %>/",
      dest: "<%= rsync.prod.public %>/",
    }
  },
  push_prod_assets: {
    options: {
      delete: true,
      src: "<%= rsync.local.assets %>/",
      dest: "<%= rsync.prod.assets %>/",
    }
  },
  pull_prod_uploads: {
    options: {
      src: "<%= rsync.prod.uploads %>/",
      dest: "<%= rsync.local.uploads %>/",
    }
  },
};
