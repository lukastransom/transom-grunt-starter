/**
 * JS files to Combine, order is important
 */
/* global module */
module.exports = {
  options: {
    stripBanners: true
  },
  dist: {
    src: [
          // Import order is important!
          // We want our Libraries, Plugins, and Modules
          // in place before we initialize them.
          // Bower Componetns
          'bower_components/jquery/dist/jquery.js',
          'bower_components/fastclick/lib/fastclick.js',
          'bower_components/transom-breakpoint/TransomBreakpoint.js',
          // Plugins
          '<%= globalConfig.src %>/javascripts/plugins/**/*.js',
          // Modules
          '<%= globalConfig.src %>/javascripts/modules/**/*.js',
          // Init
          '<%= globalConfig.src %>/javascripts/init.js'
         ],
    dest: '<%= globalConfig.dest %>/<%= globalConfig.dest_javascripts %>/<%= globalConfig.dest_javascripts_filename %>.js'
  }
};