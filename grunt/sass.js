module.exports = {
  dist: {
    options: {
      style: 'compressed',
      sourceMap: false,
      unixNewlines: true,
    },
    files: [{
      expand: true,
      cwd: '<%= globalConfig.src %>/sass',
      src: ['*.scss'],
      dest: '<%= globalConfig.dest %>/<%= globalConfig.dest_stylesheets %>/',
      ext: '.css'
    }]
  }
};