module.exports = {
  all: [
    '<%= globalConfig.dest %>/'
  ],
  fonts: [
	  '<%= globalConfig.dest %>/fonts/'
  ],
  images: [
	  '<%= globalConfig.dest %>/<%= globalConfig.dest_images %>/'
  ]
};